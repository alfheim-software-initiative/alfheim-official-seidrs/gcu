pub use crate::gcu_release;

pub fn gcu_rel() -> Result<(), Box<dyn std::error::Error>> {
    println!("=>  Creating Work Directory");
    gcu_release::create_wk_dir()?;
    gcu_release::cd_into_wkdir();
    println!("=>  Downloading Chrome Deb Package");
    gcu_release::get_package()?;
    println!("=>  Extracting deb Package");
    gcu_release::deb_extract();
    println!("=>  Extracting data tarball");
    gcu_release::xz_extract();
    println!("=>  Starting initial cleanup");
    gcu_release::cleanup()?;
    println!("=>  Adding additional files");
    gcu_release::addl_files()?;
    println!("=>  Installing the necessary files into work directory");
    gcu_release::install()?;
    println!("=>  Performing final installation");
    gcu_release::final_install()?;
    gcu_release::cd_from_wkdir();

    Ok(())
}

