extern crate atar;
extern crate azpaq;
extern crate dirsvcs;
extern crate awget;
extern crate ased;
extern crate pkgext;

use atar::extract_tar;
use azpaq::compress_zpaq;
use dirsvcs::cd;
use dirsvcs::dir;
use dirsvcs::cp;
use dirsvcs::chmod;
use awget::wget;
use ased::sed;
use pkgext::deb;

use std::fs::File;
use std::io::Write;

pub fn create_wk_dir() -> std::io::Result<()> {
    dir::mkdir("google-chrome-unstable")?;

    Ok(())
}

pub fn get_package() -> Result<(), Box<dyn std::error::Error>> {
    wget::w("https://dl.google.com/linux/direct/google-chrome-unstable_current_amd64.deb", "google-chrome-unstable_current_amd64.deb")?;

    Ok(())
}

pub fn xz_extract() {
    extract_tar::extract_xz("data.tar.xz");
}

pub fn cd_into_wkdir() {
    cd::cd("google-chrome-unstable");
}

pub fn cd_from_wkdir() {
    cd::cd("../");
}

pub fn deb_extract() {
    deb::extract("google-chrome-unstable_current_amd64.deb");
}

pub fn cleanup() -> std::io::Result<()> {
    dir::rmstr("control.tar.gz")?;
    dir::rmstr("data.tar.xz")?;
    dir::rmstr("debian-binary")?;
    dir::rmstr("google-chrome-unstable_current_amd64.deb")?;

    Ok(())
}

pub fn addl_files() -> Result<(), Box<dyn std::error::Error>> {
    let mut f = File::create("google-chrome-unstable.sh")?;
    write!(f, "#!/bin/bash

                if [[ -f ~/.config/chrome-dev-flags.conf ]]; then
                    CHROME_USER_FLAGS=\"$(cat ~/.config/chrome-dev-flags.conf)\"
                fi

                exec /opt/google/chrome-unstable/google-chrome-unstable $CHROME_USER_FLAGS \"$@\"")?;

    wget::w("https://www.google.com/intl/en_sg/chrome/privacy/eula_text.html", "eula_text.html")?;

    Ok(())
}

pub fn install() -> std::io::Result<()> {
    print!("=>  Creating License Directory");
    dir::mkdir("usr/share/licenses/google-chrome-unstable")?;

    println!("Changing mode of EULA");
    chmod::cm(0o644, "eula_text.html")?;

    println!("=>  Changing mode of google-chrome-unstable.sh");
    chmod::cm(0o755, "google-chrome-unstable.sh")?;

    println!("=>  Creating icons directories");
    dir::mkdir("usr/share/icons/hicolor/16x16/apps")?;
    dir::mkdir("usr/share/icons/hicolor/24x24/apps")?;
    dir::mkdir("usr/share/icons/hicolor/32x32/apps")?;
    dir::mkdir("usr/share/icons/hicolor/48x48/apps")?;
    dir::mkdir("usr/share/icons/hicolor/64x64/apps")?;
    dir::mkdir("usr/share/icons/hicolor/128x128/apps")?;
    dir::mkdir("usr/share/icons/hicolor/256x256/apps")?;

    println!("=>  Copying shell script and EULA into place");
    dir::rmstr("usr/bin/google-chrome-unstable")?;
    cp::cp_file("google-chrome-unstable.sh", "usr/bin/google-chrome-unstable")?;
    cp::cp_file("eula_text.html", "usr/share/licenses/google-chrome-unstable/eula_text.html")?;

    println!("=>  Copying icons into place");
    cp::cp_file("opt/google/chrome-unstable/product_logo_16_dev.png", "usr/share/icons/hicolor/16x16/apps/google-chrome-unstable.png")?;
    cp::cp_file("opt/google/chrome-unstable/product_logo_24_dev.png", "usr/share/icons/hicolor/24x24/apps/google-chrome-unstable.png")?;
    cp::cp_file("opt/google/chrome-unstable/product_logo_32_dev.png", "usr/share/icons/hicolor/32x32/apps/google-chrome-unstable.png")?;
    cp::cp_file("opt/google/chrome-unstable/product_logo_48_dev.png", "usr/share/icons/hicolor/48x48/apps/google-chrome-unstable.png")?;
    cp::cp_file("opt/google/chrome-unstable/product_logo_64_dev.png", "usr/share/icons/hicolor/64x64/apps/google-chrome-unstable.png")?;
    cp::cp_file("opt/google/chrome-unstable/product_logo_128_dev.png", "usr/share/icons/hicolor/128x128/apps/google-chrome-unstable.png")?;
    cp::cp_file("opt/google/chrome-unstable/product_logo_256_dev.png", "usr/share/icons/hicolor/256x256/apps/google-chrome-unstable.png")?;

    println!("=>  Changing mode of icons");
    chmod::cm(0o644, "usr/share/icons/hicolor/16x16/apps/google-chrome-unstable.png")?;
    chmod::cm(0o644, "usr/share/icons/hicolor/24x24/apps/google-chrome-unstable.png")?;
    chmod::cm(0o644, "usr/share/icons/hicolor/32x32/apps/google-chrome-unstable.png")?;
    chmod::cm(0o644, "usr/share/icons/hicolor/48x48/apps/google-chrome-unstable.png")?;
    chmod::cm(0o644, "usr/share/icons/hicolor/64x64/apps/google-chrome-unstable.png")?;
    chmod::cm(0o644, "usr/share/icons/hicolor/128x128/apps/google-chrome-unstable.png")?;
    chmod::cm(0o644, "usr/share/icons/hicolor/256x256/apps/google-chrome-unstable.png")?;

    println!("=>  Fixing Chrome icon resolution");
    sed::s("-i", "\"/Exec=/i/StartupWMClass=Google-chrome-unstable\"", "usr/share/applications/google-chrome-unstable.desktop");

    println!("=> Removing files not necessary on Alfheim");
    dir::rmdir("etc/cron.daily");
    dir::rmdir("opt/google/chrome-unstable/cron");
    dir::rmstr("opt/google/chrome-unstable/product_logo_*.png")?;

    Ok(())
}

pub fn final_install() -> std::io::Result<()> {
    compress_zpaq::compress("google_chrome_unstable.ap", "etc");
    compress_zpaq::compress("google_chrome_unstable.ap", "opt");
    compress_zpaq::compress("google_chrome_unstable.ap", "usr");

    println!("=>  Performing some additional cleanup");
    dir::rmdir("etc");
    dir::rmdir("opt");
    dir::rmdir("usr");
    dir::rmstr("eula_text.html")?;
    dir::rmstr("google-chrome-unstable.sh")?;

    Ok(())
}
